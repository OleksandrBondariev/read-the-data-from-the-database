<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <table border="1">
            <?php
            $result = $mysqli->query($query_1);
            while ($row = $result->fetch_assoc()) {
                echo '<tr><td>' . $row['name'] . '</td>';
                $pattern = array('/^/', '/\s/','/;/');
                $replacement = array('<td>', '</td><td>', '');
                echo preg_replace($pattern, $replacement, $row['group']);
            }
            ?>
        </table>
    </body>
</html>
