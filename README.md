
```
#!php

Есть база данных с таблицами:

tb_accounts
AccountID	AcountName
1	Revenues
2	Personal Costs and “bonuses”
3	Amortisation
_________________________________________
tb_results
Date	AccountID	Value
01.2010	1	10
01.2010	2	11
01.2010	3	12
02.2010	1	16
02.2010	2	15
02.2010	3	14
03.2010	1	67
03.2010	2	45
03.2010	3	35

Задание: нужно считать данные из базы данных в массив и вывести их (из массива) на экран в формате:
	01.2010	02.2010	03.2010
Revenues	х	х	х
Personal Costs	х	х	х
Amortisation	х	х	х
Summa	3x	3x	3x
```