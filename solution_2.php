<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <table border="1">
            <?php
            $result = $mysqli->query($query_2);
            while ($row = $result->fetch_assoc()) {
                foreach ($row as $key => $value) {
                    echo '<tr><td>' . $key . '</td>';
                    $pattern = array('/^/', '/\s/', '/;/');
                    $replacement = array('<td>', '</td><td>', '');
                    echo preg_replace($pattern, $replacement, $value);
                }
            }
            ?>
        </table>
    </body>
</html>
